﻿namespace GoBot.IHM.Pages
{
    partial class PageMatch
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PageMatch));
            this.btnTrap = new System.Windows.Forms.Button();
            this.btnColorRight = new System.Windows.Forms.Button();
            this.btnColorLeft = new System.Windows.Forms.Button();
            this.btnCalib = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.picTeensy = new System.Windows.Forms.PictureBox();
            this.picServo6 = new System.Windows.Forms.PictureBox();
            this.picServo2 = new System.Windows.Forms.PictureBox();
            this.picCAN = new System.Windows.Forms.PictureBox();
            this.picServo4 = new System.Windows.Forms.PictureBox();
            this.picServo3 = new System.Windows.Forms.PictureBox();
            this.picServo1 = new System.Windows.Forms.PictureBox();
            this.picServo5 = new System.Windows.Forms.PictureBox();
            this.picIO = new System.Windows.Forms.PictureBox();
            this.picMove = new System.Windows.Forms.PictureBox();
            this.picLidar2 = new System.Windows.Forms.PictureBox();
            this.picLidar1 = new System.Windows.Forms.PictureBox();
            this.picColor = new System.Windows.Forms.PictureBox();
            this.picCalibration = new System.Windows.Forms.PictureBox();
            this.picJack = new System.Windows.Forms.PictureBox();
            this.picTable = new System.Windows.Forms.PictureBox();
            this.picAlim = new System.Windows.Forms.PictureBox();
            this.lblAlim = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picTeensy)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCAN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIO)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLidar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLidar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picColor)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCalibration)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picJack)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAlim)).BeginInit();
            this.SuspendLayout();
            // 
            // btnTrap
            // 
            this.btnTrap.Location = new System.Drawing.Point(314, 610);
            this.btnTrap.Name = "btnTrap";
            this.btnTrap.Size = new System.Drawing.Size(75, 23);
            this.btnTrap.TabIndex = 81;
            this.btnTrap.Text = "Trap";
            this.btnTrap.UseVisualStyleBackColor = true;
            // 
            // btnColorRight
            // 
            this.btnColorRight.Font = new System.Drawing.Font("Eras Demi ITC", 36F);
            this.btnColorRight.Location = new System.Drawing.Point(158, 166);
            this.btnColorRight.Margin = new System.Windows.Forms.Padding(0, 3, 3, 3);
            this.btnColorRight.Name = "btnColorRight";
            this.btnColorRight.Size = new System.Drawing.Size(140, 100);
            this.btnColorRight.TabIndex = 82;
            this.btnColorRight.Text = "leur";
            this.btnColorRight.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnColorRight.UseVisualStyleBackColor = true;
            this.btnColorRight.Click += new System.EventHandler(this.btnColorRight_Click);
            // 
            // btnColorLeft
            // 
            this.btnColorLeft.Font = new System.Drawing.Font("Eras Demi ITC", 36F);
            this.btnColorLeft.ForeColor = System.Drawing.Color.White;
            this.btnColorLeft.Location = new System.Drawing.Point(19, 166);
            this.btnColorLeft.Margin = new System.Windows.Forms.Padding(3, 3, 0, 3);
            this.btnColorLeft.Name = "btnColorLeft";
            this.btnColorLeft.Size = new System.Drawing.Size(140, 100);
            this.btnColorLeft.TabIndex = 83;
            this.btnColorLeft.Text = "Cou";
            this.btnColorLeft.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnColorLeft.UseVisualStyleBackColor = true;
            this.btnColorLeft.Click += new System.EventHandler(this.btnColorLeft_Click);
            // 
            // btnCalib
            // 
            this.btnCalib.BackColor = System.Drawing.Color.Transparent;
            this.btnCalib.Enabled = false;
            this.btnCalib.Font = new System.Drawing.Font("Eras Demi ITC", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCalib.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(30)))), ((int)(((byte)(30)))));
            this.btnCalib.Location = new System.Drawing.Point(19, 451);
            this.btnCalib.Name = "btnCalib";
            this.btnCalib.Size = new System.Drawing.Size(280, 100);
            this.btnCalib.TabIndex = 85;
            this.btnCalib.Text = "Recalage";
            this.btnCalib.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCalib.UseVisualStyleBackColor = false;
            this.btnCalib.Click += new System.EventHandler(this.btnCalib_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Eras Demi ITC", 36F);
            this.label1.Location = new System.Drawing.Point(66, 331);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(174, 55);
            this.label1.TabIndex = 98;
            this.label1.Text = "Tirette";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(8, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 18);
            this.label2.TabIndex = 102;
            this.label2.Text = "Lidar Adv.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(85, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(73, 18);
            this.label3.TabIndex = 103;
            this.label3.Text = "Lidar Sol";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(162, 34);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 18);
            this.label4.TabIndex = 104;
            this.label4.Text = "Move";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(316, 34);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(73, 18);
            this.label5.TabIndex = 105;
            this.label5.Text = "CAN";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(393, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 18);
            this.label6.TabIndex = 106;
            this.label6.Text = "Servo 1";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(470, 34);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 18);
            this.label7.TabIndex = 107;
            this.label7.Text = "Servo 2";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(547, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(73, 18);
            this.label8.TabIndex = 108;
            this.label8.Text = "Servo 3";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(624, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(73, 18);
            this.label9.TabIndex = 109;
            this.label9.Text = "Servo 4";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(701, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 18);
            this.label10.TabIndex = 110;
            this.label10.Text = "Servo 5";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(778, 34);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(73, 18);
            this.label11.TabIndex = 111;
            this.label11.Text = "Servo 6";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(239, 34);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(73, 18);
            this.label12.TabIndex = 112;
            this.label12.Text = "IO";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(932, 34);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(73, 18);
            this.label13.TabIndex = 113;
            this.label13.Text = "Teensy";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picTeensy
            // 
            this.picTeensy.Image = ((System.Drawing.Image)(resources.GetObject("picTeensy.Image")));
            this.picTeensy.Location = new System.Drawing.Point(944, 55);
            this.picTeensy.Name = "picTeensy";
            this.picTeensy.Size = new System.Drawing.Size(48, 48);
            this.picTeensy.TabIndex = 125;
            this.picTeensy.TabStop = false;
            // 
            // picServo6
            // 
            this.picServo6.Image = ((System.Drawing.Image)(resources.GetObject("picServo6.Image")));
            this.picServo6.Location = new System.Drawing.Point(790, 55);
            this.picServo6.Name = "picServo6";
            this.picServo6.Size = new System.Drawing.Size(48, 48);
            this.picServo6.TabIndex = 124;
            this.picServo6.TabStop = false;
            // 
            // picServo2
            // 
            this.picServo2.Image = global::GoBot.Properties.Resources.ValidNok48;
            this.picServo2.Location = new System.Drawing.Point(482, 55);
            this.picServo2.Name = "picServo2";
            this.picServo2.Size = new System.Drawing.Size(48, 48);
            this.picServo2.TabIndex = 123;
            this.picServo2.TabStop = false;
            // 
            // picCAN
            // 
            this.picCAN.Image = global::GoBot.Properties.Resources.ValidNok48;
            this.picCAN.Location = new System.Drawing.Point(328, 55);
            this.picCAN.Name = "picCAN";
            this.picCAN.Size = new System.Drawing.Size(48, 48);
            this.picCAN.TabIndex = 122;
            this.picCAN.TabStop = false;
            // 
            // picServo4
            // 
            this.picServo4.Image = ((System.Drawing.Image)(resources.GetObject("picServo4.Image")));
            this.picServo4.Location = new System.Drawing.Point(636, 55);
            this.picServo4.Name = "picServo4";
            this.picServo4.Size = new System.Drawing.Size(48, 48);
            this.picServo4.TabIndex = 121;
            this.picServo4.TabStop = false;
            // 
            // picServo3
            // 
            this.picServo3.Image = ((System.Drawing.Image)(resources.GetObject("picServo3.Image")));
            this.picServo3.Location = new System.Drawing.Point(559, 55);
            this.picServo3.Name = "picServo3";
            this.picServo3.Size = new System.Drawing.Size(48, 48);
            this.picServo3.TabIndex = 120;
            this.picServo3.TabStop = false;
            // 
            // picServo1
            // 
            this.picServo1.Image = ((System.Drawing.Image)(resources.GetObject("picServo1.Image")));
            this.picServo1.Location = new System.Drawing.Point(405, 55);
            this.picServo1.Name = "picServo1";
            this.picServo1.Size = new System.Drawing.Size(48, 48);
            this.picServo1.TabIndex = 119;
            this.picServo1.TabStop = false;
            // 
            // picServo5
            // 
            this.picServo5.Image = ((System.Drawing.Image)(resources.GetObject("picServo5.Image")));
            this.picServo5.Location = new System.Drawing.Point(713, 55);
            this.picServo5.Name = "picServo5";
            this.picServo5.Size = new System.Drawing.Size(48, 48);
            this.picServo5.TabIndex = 118;
            this.picServo5.TabStop = false;
            // 
            // picIO
            // 
            this.picIO.Image = ((System.Drawing.Image)(resources.GetObject("picIO.Image")));
            this.picIO.Location = new System.Drawing.Point(251, 55);
            this.picIO.Name = "picIO";
            this.picIO.Size = new System.Drawing.Size(48, 48);
            this.picIO.TabIndex = 117;
            this.picIO.TabStop = false;
            // 
            // picMove
            // 
            this.picMove.Image = ((System.Drawing.Image)(resources.GetObject("picMove.Image")));
            this.picMove.Location = new System.Drawing.Point(174, 55);
            this.picMove.Name = "picMove";
            this.picMove.Size = new System.Drawing.Size(48, 48);
            this.picMove.TabIndex = 116;
            this.picMove.TabStop = false;
            // 
            // picLidar2
            // 
            this.picLidar2.Image = ((System.Drawing.Image)(resources.GetObject("picLidar2.Image")));
            this.picLidar2.Location = new System.Drawing.Point(97, 55);
            this.picLidar2.Name = "picLidar2";
            this.picLidar2.Size = new System.Drawing.Size(48, 48);
            this.picLidar2.TabIndex = 115;
            this.picLidar2.TabStop = false;
            // 
            // picLidar1
            // 
            this.picLidar1.Image = ((System.Drawing.Image)(resources.GetObject("picLidar1.Image")));
            this.picLidar1.Location = new System.Drawing.Point(20, 55);
            this.picLidar1.Name = "picLidar1";
            this.picLidar1.Size = new System.Drawing.Size(48, 48);
            this.picLidar1.TabIndex = 114;
            this.picLidar1.TabStop = false;
            // 
            // picColor
            // 
            this.picColor.Location = new System.Drawing.Point(304, 168);
            this.picColor.Name = "picColor";
            this.picColor.Size = new System.Drawing.Size(96, 96);
            this.picColor.TabIndex = 101;
            this.picColor.TabStop = false;
            // 
            // picCalibration
            // 
            this.picCalibration.Image = global::GoBot.Properties.Resources.ValidNok96;
            this.picCalibration.Location = new System.Drawing.Point(304, 450);
            this.picCalibration.Name = "picCalibration";
            this.picCalibration.Size = new System.Drawing.Size(96, 96);
            this.picCalibration.TabIndex = 100;
            this.picCalibration.TabStop = false;
            // 
            // picJack
            // 
            this.picJack.Image = global::GoBot.Properties.Resources.ValidNok96;
            this.picJack.Location = new System.Drawing.Point(304, 309);
            this.picJack.Name = "picJack";
            this.picJack.Size = new System.Drawing.Size(96, 96);
            this.picJack.TabIndex = 99;
            this.picJack.TabStop = false;
            // 
            // picTable
            // 
            this.picTable.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.picTable.Location = new System.Drawing.Point(404, 156);
            this.picTable.Name = "picTable";
            this.picTable.Size = new System.Drawing.Size(612, 410);
            this.picTable.TabIndex = 0;
            this.picTable.TabStop = false;
            // 
            // picAlim
            // 
            this.picAlim.Image = ((System.Drawing.Image)(resources.GetObject("picAlim.Image")));
            this.picAlim.Location = new System.Drawing.Point(867, 55);
            this.picAlim.Name = "picAlim";
            this.picAlim.Size = new System.Drawing.Size(48, 48);
            this.picAlim.TabIndex = 127;
            this.picAlim.TabStop = false;
            // 
            // lblAlim
            // 
            this.lblAlim.Font = new System.Drawing.Font("Eras Medium ITC", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAlim.Location = new System.Drawing.Point(855, 34);
            this.lblAlim.Name = "lblAlim";
            this.lblAlim.Size = new System.Drawing.Size(73, 18);
            this.lblAlim.TabIndex = 126;
            this.lblAlim.Text = "Alim";
            this.lblAlim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PageMatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.picAlim);
            this.Controls.Add(this.lblAlim);
            this.Controls.Add(this.picTeensy);
            this.Controls.Add(this.picServo6);
            this.Controls.Add(this.picServo2);
            this.Controls.Add(this.picCAN);
            this.Controls.Add(this.picServo4);
            this.Controls.Add(this.picServo3);
            this.Controls.Add(this.picServo1);
            this.Controls.Add(this.picServo5);
            this.Controls.Add(this.picIO);
            this.Controls.Add(this.picMove);
            this.Controls.Add(this.picLidar2);
            this.Controls.Add(this.picLidar1);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.picColor);
            this.Controls.Add(this.picCalibration);
            this.Controls.Add(this.picJack);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnColorRight);
            this.Controls.Add(this.btnColorLeft);
            this.Controls.Add(this.btnCalib);
            this.Controls.Add(this.btnTrap);
            this.Controls.Add(this.picTable);
            this.DoubleBuffered = true;
            this.Name = "PageMatch";
            this.Size = new System.Drawing.Size(1022, 598);
            this.Load += new System.EventHandler(this.PageMatch_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picTeensy)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCAN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picServo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picIO)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLidar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLidar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picColor)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picCalibration)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picJack)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picAlim)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picTable;
        private System.Windows.Forms.Button btnTrap;
        private System.Windows.Forms.Button btnColorRight;
        private System.Windows.Forms.Button btnColorLeft;
        private System.Windows.Forms.Button btnCalib;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox picJack;
        private System.Windows.Forms.PictureBox picCalibration;
        private System.Windows.Forms.PictureBox picColor;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox picLidar1;
        private System.Windows.Forms.PictureBox picLidar2;
        private System.Windows.Forms.PictureBox picMove;
        private System.Windows.Forms.PictureBox picIO;
        private System.Windows.Forms.PictureBox picServo5;
        private System.Windows.Forms.PictureBox picServo1;
        private System.Windows.Forms.PictureBox picServo3;
        private System.Windows.Forms.PictureBox picServo4;
        private System.Windows.Forms.PictureBox picCAN;
        private System.Windows.Forms.PictureBox picServo2;
        private System.Windows.Forms.PictureBox picServo6;
        private System.Windows.Forms.PictureBox picTeensy;
        private System.Windows.Forms.PictureBox picAlim;
        private System.Windows.Forms.Label lblAlim;
    }
}
